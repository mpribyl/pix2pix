import argparse
import json
import os
import time
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from dotmap import DotMap

from dataset import create_dataset
from loss import generator_loss, discriminator_loss
from models import Generator, Discriminator


def process_training_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    group = ap.add_argument_group('dataset options')
    group.add_argument("--input_images_dir",
                       type=str, required=True,
                       help="Path to input images")
    group.add_argument("--output_images_dir",
                       type=str, required=True,
                       help="Path to output images")

    group = ap.add_argument_group('training options')
    group.add_argument("--batch_size",
                       type=int, default=32,
                       help="Size of training batch size")
    group.add_argument("--gen_lr",
                       type=float, default=2e-4,
                       help="Learning rate of generator's optimizer")
    group.add_argument("--dis_lr",
                       type=float, default=2e-4,
                       help="Learning rate of discriminator's optimizer")
    group.add_argument("--epochs",
                       type=int, default=1,
                       help="How much epochs train the model.")
    group.add_argument("--log_dir",
                       type=str, default='./logs/',
                       help="Logging directory.")
    group.add_argument("--img_width",
                       type=int, default=256,
                       help="Image width.")
    group.add_argument("--img_height",
                       type=int, default=256,
                       help="Image height.")
    group.add_argument("--generate_images_freq",
                       type=int, default=100,
                       help="Generate images every x-th batch.")
    group.add_argument("--saved_models_dir",
                       type=str, default='./saved_models/',
                       help="Directory where will be saved models.")
    group.add_argument("--save_model_freq",
                       type=int, default=100,
                       help="Saves model every x-th batch.")
    group.add_argument("--saved_checkpoints_dir",
                       type=str, default='./saved_checkpoints/',
                       help="Directory where will be saved checkpoints.")
    group.add_argument("--save_checkpoint_freq",
                       type=int, default=100,
                       help="Saves checkpoint every x-th batch.")
    group.add_argument("--load_checkpoint",
                       type=str, default=None,
                       help="Path to checkpoint which will be load.")
    group.add_argument("--input_channels",
                       type=int, default=1,
                       help="Num channels of input images.")

    return DotMap(vars(ap.parse_args()))


def get_filenames(dir_name):
    filenames = []
    for filename in os.listdir(dir_name):
        if os.path.isfile(os.path.join(dir_name, filename)):
            filenames.append(filename)
    return filenames


def create_training_params(input_args) -> DotMap:
    params = DotMap(input_args)

    params.steps_per_epoch = np.ceil(len(get_filenames(params.input_images_dir)) / params.batch_size)

    params_for_print = params
    print('Training params:')
    print(json.dumps(params_for_print, indent=2))

    os.makedirs(params.log_dir, exist_ok=True)
    with open(os.path.join(params.log_dir, 'input_params.json'), 'w') as f:
        json.dump(params_for_print, f, indent=2)

    return params


def save_generated_image(model, input_image, output_image, output_path, step):
    prediction = model(input_image, training=True)

    images = [input_image, output_image, prediction]

    nrows = min(len(input_image), 10)
    ncols = 3
    fig, axs = plt.subplots(nrows, ncols, squeeze=False, figsize=(6, 15))
    fig.suptitle('Input Image, Ground Truth, Predicted Image', fontsize=14)

    for row in range(nrows):
        for col in range(ncols):
            img = images[col][row]
            if img.shape[2] == 1:
                img = np.squeeze(img)
            axs[row, col].imshow(img * 0.5 + 0.5)
            axs[row, col].axis('off')

    plt.tight_layout()
    plt.savefig(os.path.join(output_path, 'generated_image_step_{}.jpg'.format(step)))
    plt.clf()
    plt.close('all')


def train_step_wrapper(
        # models
        generator, discriminator,
        # loss functions
        gen_loss_fn, dis_loss_fn,
        # optimizers
        gen_opt, dis_opt):
    @tf.function
    def train_step(input_image, output_image):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            gen_output = generator(input_image, training=True)

            disc_real_output = discriminator([input_image, output_image], training=True)
            disc_generated_output = discriminator([input_image, gen_output], training=True)

            gen_total_loss, gen_gan_loss, gen_l1_loss = gen_loss_fn(disc_generated_output, gen_output, output_image)
            disc_loss = dis_loss_fn(disc_real_output, disc_generated_output)

        generator_gradients = gen_tape.gradient(gen_total_loss, generator.trainable_variables)
        discriminator_gradients = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

        gen_opt.apply_gradients(zip(generator_gradients, generator.trainable_variables))
        dis_opt.apply_gradients(zip(discriminator_gradients, discriminator.trainable_variables))

        return {
            "gen_total_loss": gen_total_loss,
            "gen_gan_loss": gen_gan_loss,
            "gen_l1_loss": gen_l1_loss,
            "disc_loss": disc_loss
        }

    return train_step


def main():
    inpu_args = process_training_input_arguments()
    params = create_training_params(inpu_args)

    train_ds = create_dataset(
        input_images_dir=params.input_images_dir,
        output_images_dir=params.output_images_dir,
        batch_size=params.batch_size,
        img_width=params.img_width,
        img_height=params.img_height
    )

    generator = Generator(params.input_channels)
    discriminator = Discriminator(params.input_channels)

    gen_opt = tf.keras.optimizers.Adam(params.gen_lr, beta_1=0.5)
    dis_opt = tf.keras.optimizers.Adam(params.dis_lr, beta_1=0.5)

    checkpoint = tf.train.Checkpoint(
        step=tf.Variable(0),
        generator=generator,
        discriminator=discriminator,
        gen_opt=gen_opt,
        dis_opt=dis_opt,
        epoch_iter=iter(train_ds)
    )

    if params.load_checkpoint:
        checkpoint.restore(params.load_checkpoint)

    cur_epoch = int(int(checkpoint.step) / len(train_ds))

    manager = tf.train.CheckpointManager(checkpoint, params.saved_checkpoints_dir, max_to_keep=1)

    log_dir = os.path.join(params.log_dir, datetime.now().strftime("%Y_%m_%d-%H_%M_%S"))
    file_writer = tf.summary.create_file_writer(log_dir)

    train_step_fn = train_step_wrapper(
        generator, discriminator,
        generator_loss, discriminator_loss,
        gen_opt, dis_opt
    )

    gen_total_loss = []
    gen_gan_loss = []
    gen_l1_loss = []
    disc_loss = []
    while cur_epoch < params.epochs:
        epoch_start = time.time()

        if int(checkpoint.step) % len(train_ds) == 0 and int(checkpoint.step) != 0:
            checkpoint.epoch_iter = iter(train_ds)

        for (input_images_batch, output_images_batch) in checkpoint.epoch_iter:
            step_start = time.time()
            logs = train_step_fn(input_images_batch, output_images_batch)

            gen_total_loss.append(logs['gen_total_loss'])
            gen_gan_loss.append(logs['gen_gan_loss'])
            gen_l1_loss.append(logs['gen_l1_loss'])
            disc_loss.append(logs['disc_loss'])
            with file_writer.as_default():
                tf.summary.scalar('gen_total_loss', gen_total_loss[len(gen_total_loss) - 1], step=int(checkpoint.step))
                tf.summary.scalar('gen_gan_loss', gen_gan_loss[len(gen_gan_loss) - 1], step=int(checkpoint.step))
                tf.summary.scalar('gen_l1_loss', gen_l1_loss[len(gen_l1_loss) - 1], step=int(checkpoint.step))
                tf.summary.scalar('disc_loss', disc_loss[len(disc_loss) - 1], step=int(checkpoint.step))
            print('------step: {}, time: {:.2f} sec, gen_total_loss: {}, disc_loss: {}'
                  .format(int(checkpoint.step), time.time() - step_start, gen_total_loss[len(gen_total_loss) - 1],
                          disc_loss[len(disc_loss) - 1]))

            if int(checkpoint.step) % params.generate_images_freq == 0:
                save_generated_image(generator, input_images_batch, output_images_batch, params.log_dir,
                                     int(checkpoint.step))

            checkpoint.step.assign_add(1)

            if int(checkpoint.step) % params.save_checkpoint_freq == 0 and int(checkpoint.step) != 0:
                save_path = manager.save(checkpoint_number=int(checkpoint.step))
                print("saved checkpoint for step {}: {}".format(int(checkpoint.step), save_path))

        print('epoch: {}, time: {:.2f} sec, gen_total_loss: {}, disc_loss: {}'
              .format(cur_epoch, time.time() - epoch_start, gen_total_loss[len(gen_total_loss) - 1],
                      disc_loss[len(disc_loss) - 1]))
        if cur_epoch % params.save_model_freq == 0:
            generator.save(os.path.join(params.saved_models_dir, 'epoch_{}.h5'.format(cur_epoch)), save_format='h5')
            print("saved generator for epoch {}: {}"
                  .format(cur_epoch, os.path.join(params.saved_models_dir, 'epoch_{}'.format(cur_epoch))))
        print('-------------------------------------')

        checkpoint.epoch_iter = iter(train_ds)
        cur_epoch = int(int(checkpoint.step) / len(train_ds))


if __name__ == '__main__':
    main()
