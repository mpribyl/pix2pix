import os

import tensorflow as tf

BUFFER_SIZE = 400


def load(input_image_path, output_image_path):
    input_image = tf.io.read_file(input_image_path)
    output_image = tf.io.read_file(output_image_path)

    input_image = tf.image.decode_jpeg(input_image)
    output_image = tf.image.decode_jpeg(output_image)

    input_image = tf.cast(input_image, tf.float32)
    output_image = tf.cast(output_image, tf.float32)

    return input_image, output_image


def resize(input_image, output_image, height, width):
    input_image = tf.image.resize(input_image, [height, width],
                                  method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    output_image = tf.image.resize(output_image, [height, width],
                                   method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    return input_image, output_image


def normalize(input_image, output_image):
    input_image = (input_image / 127.5) - 1
    output_image = (output_image / 127.5) - 1

    return input_image, output_image


def create_load_image_train_fn(img_width, img_height):
    @tf.function
    def load_image_train(input_image_path, output_image_path):
        input_image, output_image = load(input_image_path, output_image_path)
        input_image, output_image = resize(input_image, output_image, img_height, img_width)
        input_image, output_image = normalize(input_image, output_image)
        return input_image, output_image

    return load_image_train


def create_dataset(input_images_dir, output_images_dir, batch_size, img_width, img_height):
    input_images_paths = tf.data.Dataset.list_files(os.path.join(input_images_dir, '*.jpg'), shuffle=False)
    output_images_paths = tf.data.Dataset.list_files(os.path.join(output_images_dir, '*.jpg'), shuffle=False)

    train_ds = tf.data.Dataset.zip((input_images_paths, output_images_paths))
    train_ds = train_ds.map(create_load_image_train_fn(img_width, img_height), num_parallel_calls=tf.data.AUTOTUNE)
    train_ds = train_ds.shuffle(BUFFER_SIZE)
    train_ds = train_ds.batch(batch_size)
    train_ds = train_ds.prefetch(buffer_size=tf.data.AUTOTUNE)

    return train_ds
