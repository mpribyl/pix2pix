# Pix2Pix
Implementace Pix2Pix modelu, který generuje 
dermoskopické obrázky z [datasetu ISIC 2019](https://challenge2019.isic-archive.com/). Část kódu vychází 
z  [notebooku](https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/pix2pix.ipynb) od Tensorflow.

## Model ke stažení
Model je ke stažení na adrese [https://drive.google.com/file/d/1V--kluHcqlkfOrul53qFpnW1I6JrIzQf/view?usp=sharing](https://drive.google.com/file/d/1V--kluHcqlkfOrul53qFpnW1I6JrIzQf/view?usp=sharing). Tento model byl trénován po dobu 30 epoch.

## Dataset
Model vyžaduje mít vstupní a výstupní obrázky v oddělených složkách. 
Názvy obrázků v těchto obou složkách musí být shodné viz příklad níže:

* `./dataset/input_images/img1.jpg, ./dataset/input_images/img2.jpg, ...`
* `./dataset/output_images/img1.jpg, ./dataset/output_images/img2.jpg, ...`

## Trénování

* spustit trénování:

```bash
python train.py \
--input_images_dir ./dataset/input_images/ \
--output_images_dir ./dataset/output_images/ \
--batch_size 64 \
--epochs 30 \
--generate_images_freq 396 \
--save_model_freq 1 \
--save_checkpoint_freq 132 \
--log_dir ./logs \
--saved_models_dir ./logs/saved_models \
--saved_checkpoints_dir ./logs/saved_checkpoints
```

* obnovit trénování:

```bash
python train.py \
--input_images_dir ./dataset/input_images/ \
--output_images_dir ./dataset/output_images/ \
--batch_size 64 \
--epochs 30 \
--generate_images_freq 396 \
--save_model_freq 1 \
--save_checkpoint_freq 132 \
--log_dir ./logs \
--saved_models_dir ./logs/saved_models \
--saved_checkpoints_dir ./logs/saved_checkpoints \
--load_checkpoint ./logs/saved_checkpoints/ckpt-462
```
